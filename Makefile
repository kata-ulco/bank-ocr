.PHONY: up do ex

SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

rebuild:
	docker build --no-cache -t ulco-bank .

build:
	docker build -t ulco-bank .

up:
	docker run -it -d --env-file ./docker/xdebug.env --name ulco-bank -v $(shell pwd):/app -v $(shell pwd)/docker/xdebug.ini:/usr/local/etc/php/conf.d/docker-php-ext-xdebug-20.ini:ro -w=/app ulco-bank
	docker exec -it ulco-bank sh ./docker/setup-xdebug.sh
	docker exec -it ulco-bank composer install

up-w:
	docker run -it -d --env-file ./docker/xdebug.env --name ulco -v ${CURDIR}:/app -v ${CURDIR}/docker/xdebug.ini:/usr/local/etc/php/conf.d/docker-php-ext-xdebug-20.ini:ro -w=/app ulco-bank
	docker exec -it ulco-bank sh ./docker/setup-xdebug.sh
	docker exec -it ulco-bank composer install

do:
	docker rm -vf ulco-bank

ex:
	docker exec -u $(CURRENT_UID):$(CURRENT_GID) -it ulco-bank sh
